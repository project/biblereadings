<?php print $name; ?>,

Here are your daily Bible readings for <?php print date('l, F j, Y'); ?>, provided by 
Family Community Church.


TODAY'S READINGS:
* <?php print biblereadings_format_psalms($readings); ?>  
* <?php print biblereadings_format_proverb($readings); ?>  
* <?php print biblereadings_format_regular_reading($readings); ?>  

Visit www.FamilyCC.org to download the full "Read the Bible in a Year" plan.

May your day be filled and guided by the Blessing of the Lord!

Family Community Church

6331 Watt Ave, North Highlands, CA 95660
phone: (916) 334-7700
email: info@FamilyCC.org
web: http://www.FamilyCC.org

To unsubscribe from this email update, folow the link below.
<?php print url('biblereadings/unsubscribe', array('query' => array('email' => $email), 'absolute' => TRUE)); ?>
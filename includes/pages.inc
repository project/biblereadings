<?php

function biblereadings_page_schedule($year = NULL) {
  $output = '';

  // Default year to current year if not set to another number
  if (!$year || !is_numeric($year)) {
    $year = date('Y');
  }

  $start = strtotime('1/1/' . $year);
  $end = strtotime('12/31/' . $year . ' 23:59:59');

  $readings = biblereadings_readings_by_date_range($start, $end);
  $months = array();

  foreach($readings as $date => $r) {
    $month = date('F, Y', $date);
    if (!isset($months[$month])) {
      $months[$month] = array();
    }

    $months[$month][$date] = $r;
  }

  $output = theme('biblereadings_full_year_schedule', array('readings_by_month' => $months));

  return $output;
}

function biblereadings_page_unsubscribe() {
  $email = $_GET['email'];
  $output = '';

  if ($email) {

    if (biblereadings_subscription_exists($email)) {
      biblereadings_unsubscribe($email);

      $output .= t('!email has been unsubscribed from daily Bible readings.', array('!email' => $email));
    }
    else {
      $output .= t('There is no subscription for !email.', array('!email' => $email));
    }
  }
  else {
    $output .= t('No email address was specified.');
  }


  return $output;
}
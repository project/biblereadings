<?php

function template_preprocess_biblereadings_daily_email_html(&$vars) {

}

function template_preprocess_biblereadings_daily_email_text(&$vars) {

}

function theme_biblereadings_full_year_schedule(&$vars) {
  $readings_by_month = $vars['readings_by_month'];

  $path = drupal_get_path('module', 'biblereadings');
  drupal_add_css("$path/css/biblereadings.css");

  $output = '';
  $dt_format = 'l, F j';
  $version = 'NKJV';
  $show_link = TRUE;
  $abbreviated = FALSE;
  $today = strtotime(date('Y-m-d'));

  foreach ($readings_by_month as $month => $days) {
    $output .= '<h3>' . t($month) . '</h3>';

    $head = array(t('Day'), t('Psalms'), t('Proverbs'), t('Readings'));
    $rows = array();

    foreach ($days as $date => $r) {

      $class = (date('Y-m-d', $date) == date('Y-m-d', $today)) ? 'today' : '';

      $regular = [];
      foreach ($r->readings as $i) {
        $regular[] = biblereadings_format_regular_reading($i, $abbreviated, $show_link, $version);
      }

      $psalms = biblereadings_format_psalms($r, $abbreviated, $show_link, $version);
      $proverbs = biblereadings_format_proverb($r, $abbreviated, $show_link, $version);

      $rows[] = array(
        'class' => array($class),
        'data' => array(
          date($dt_format, $date),
          $psalms,
          $proverbs,
          implode(', ', $regular),
        ),
      );
    }

    $output .= theme('table', array(
      'header' => $head,
      'rows' => $rows,
      'attributes' => array('id' => 'biblereadings-this-week')
    ));

  }

  return $output;
}

/**
 * @todo Please document this function.
 * @see http://drupal.org/node/1354
 */
function theme_biblereadings_readings_this_week(&$vars) {
  $readings = $vars['readings'];
  $show_downloads = $vars['show_downloads'];
  if (isset($vars['abbreviated'])) {
    $abbreviated = $vars['abbreviated'];
  }
  else {
    $abbreviated = '';
  }

  $path = drupal_get_path('module', 'biblereadings');
  drupal_add_css("$path/css/biblereadings.css");

  $today = strtotime(date('Y-m-d'));

  $output = '';

  $dt_format = 'l';
  $version = 'NKJV';
  $show_link = TRUE;

  $head = array(t('Day'), t('Devotions'), t('Readings'));
  $rows = array();

  foreach ($readings as $date => $r) {
    $class = (date('Y-m-d', $date) == date('Y-m-d', $today)) ? 'today' : '';

    $regular = [];
    foreach ($r->readings as $i) {
      $regular[] = biblereadings_format_regular_reading($i, $abbreviated, $show_link, $version);
    }

    $psalms = biblereadings_format_psalms($r, $abbreviated, $show_link, $version);

    $psalms .= ' / ';

    $psalms .= biblereadings_format_proverb($r, $abbreviated, $show_link, $version);

    $rows[] = array(
      'class' => array($class),
      'data' => array(
        date($dt_format, $i->date),
        $psalms,
        implode(', ', $regular),
      ),
    );
  }

  $output .= theme('table', array(
    'header' => $head,
    'rows' => $rows,
    'attributes' => array('id' => 'biblereadings-this-week')
  ));

  // TODO: pull this out and put it in a custom module or something.
  if ($show_downloads) {
    $links = array();
    $standard = variable_get('biblereadings_standard_readings_download_path', '');
    $chronological = variable_get('biblereadings_chronological_readings_download_path', '');

    if ($standard) {
      $links[] = l('Standard Bible reading plan', file_create_url($standard));
    }
    if ($chronological) {
      $links[] = l('Chronological Bible reading plan', $chronological, array('attributes' => array('target' => '_blank')));
    }

    $output .= theme('item_list', array(
      'items' => $links,
      'title' => 'Download a Printable Reading Plan'
    ));
  }

  return $output;
}


function theme_biblereadings_readings_today(&$vars) {
  $readings = $vars['readings'];
  if (isset($vars['abbreviated'])) {
    $abbreviated = $vars['abbreviated'];
  }
  else {
    $abbreviated = '';
  }

  $path = drupal_get_path('module', 'biblereadings');
  drupal_add_css("$path/css/biblereadings.css");

  $output = '';

  $show_link = TRUE;
  $version = 'NKJV';

  $today_readings = reset($readings);


  $output .= '<p>';
  foreach ($today_readings->readings as $i) {
    $output .= biblereadings_format_regular_reading($i, $abbreviated, $show_link, $version);
    $output .= '<br>';
  }
  $output .= '</p>';
  $output .= '<p>' . biblereadings_format_psalms($today_readings, $abbreviated, $show_link, $version) . '</p>';
  $output .= '<p>' . biblereadings_format_proverb($today_readings, $abbreviated, $show_link, $version) . '</p>';


  return $output;
}
